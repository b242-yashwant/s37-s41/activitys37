const express = require ("express");
const mongoose= require("mongoose");
const cors = require("cors");

//allows our backend application to be available to our frontend application
//allows us to control the apps cross origin resource sharing settings

const app= express();

//connect to our mongodb database
mongoose.connect("mongodb+srv://yashwant:LE13OL0dnLVV0YOy@zuitt-bootcamp.bbfflap.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology:true
});
//prompts a message in the terminal once the connection is "open"
mongoose.connection.once('open',()=>console.log('now connected to mongodb atlas'));
//allows all resoureces to access our backend application
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//port number definition
app.listen(process.env.PORT||4000, () =>{
	console.log(`API is now online on port ${process.env.PORT||4000}`)
} )