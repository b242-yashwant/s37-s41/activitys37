const mongoose = require("mongoose");

const UsersSchema= new mongoose.Schema({
	firstName:{
		type: String,
		required:[true,"firstName is required"]		
	},
	
    lastName:{
    	type:String,
    	require:[true,"lastName is required"]
    },

    email:{
    	type:String,
    	required:[true,"email is required"]
    },

    password:{
    	type:String,
    	required:[true,"passwrod is required"]
    },

    isAdmin:{
    	type:String,
    	required:[true,"this field is required"]
    },

    mobile No.:{
        type: String,
        required:[true, "mobile No. is required"]
    },

    enrollments:{
    	courseId:{
    		type:String,
    		required:[true, "courseId is required"]
    	},
    	enrolledOn:{
    		type:Date,
    		default: new Date()
    	},
    	status:{
    		type:String,
    		default: Enrolled
    	}
    }
})
